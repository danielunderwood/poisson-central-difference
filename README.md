# poisson-central-difference

__NOTE:__ This repository contains code for data analysis and thus is poorly structured as it is not meant to be continually supported or updated.

### Paper Build Status
[![Build Badge](https://www.sharelatex.com/github/repos/danielunderwood/poisson-central-difference/builds/latest/badge.svg)](https://www.sharelatex.com/github/repos/danielunderwood/poisson-central-difference)

### [Download Latest Build](https://www.sharelatex.com/github/repos/danielunderwood/poisson-central-difference/builds/latest/output.pdf)
Solving poisson equation as linear system using central difference method. Written for Spring 2015 MA 428 course.
Contains code for arbitrarily creating a gridded domain with a matrix representation.
